'''

author: Michael Street

Core test package of the diagnostic test solutions

'''
from __future__ import division
from core_beginner_diagnostic import *
import unittest


#------------------------------------------
# Word Counting tests
#------------------------------------------

class TestHashedWordCount(unittest.TestCase):

    def test_hashedWordCount(self):
        sentence = "she sells she jumps sells shells"
        word = "she"
        expected = [0, 2]
        actual = hashedWordCount(sentence)
        self.assertListEqual(expected, actual[word])

        sentence = "hello help hell hello helper helped"
        word = "help"
        expected = [1]
        actual = hashedWordCount(sentence)
        self.assertListEqual(expected, actual[word])

        sentence = "she sells she jumps sells shells."
        word = "shells"
        actual = hashedWordCount(sentence)
        expected = False
        self.assertEqual(expected, word in actual.keys())       


class TestWordCounter(unittest.TestCase):
    def test_wordCounter(self):
        sentence = "she sells she jumps sells shells"
        word = "she"
        expected = [0, 2]
        actual = wordCount(sentence, word)
        self.assertListEqual(expected, actual)

        sentence = "hello help hell hello helper helped"
        word = "help"
        expected = [1]
        actual = wordCount(sentence, word)
        self.assertListEqual(expected, actual)

        sentence = "she sells she jumps sells shells."
        word = "shells"
        expected = False
        actual = wordCount(sentence, word)
        self.assertEqual(expected, actual)        

#------------------------------------------
# Polynomial evaluator tests
#------------------------------------------
class TestEvalPolynomial(unittest.TestCase):

    def test_basic1(self):
        # basic 1
        a_n = [0, 0, 0]
        x = 100000
        actual = evalPolynomial(x, a_n)
        expected = 0.0
        self.assertAlmostEqual(expected, actual)

    def test_basic2(self):
        # basic 2
        a_n = [-1, -1]
        x = 10.0
        actual = evalPolynomial(10, a_n)
        expected = -11
        self.assertAlmostEqual(expected, actual)

    def test_basic3(self):
        # basic 3
        a_n = [9, 12, 4]
        x = -2/3
        expected = 0.0
        actual = evalPolynomial(x, a_n)
        self.assertAlmostEqual(expected, actual)
        
#------------------------------------------
# Cashier tests
#------------------------------------------       
class TestCalculateChange(unittest.TestCase):

    def test_equivalent(self):
        ''' Test equivalent'''

        price = 20
        tendered = 20
        actual = calculateChange(price, tendered)
        expected = [0, 0, 0, 0]
        self.assertListEqual(actual, expected)
        
    def test_negative(self):
        '''Test negative'''
        self.assertRaises(ValueError, calculateChange, -10, 5)

        self.assertRaises(ValueError, calculateChange, 23, -9)

    def test_integer(self):
        '''Test integer'''
        self.assertRaises(ValueError, calculateChange, 8.90, 16)
        self.assertRaises(ValueError, calculateChange, 9, 19.99)

    def test_basic_1(self):
        '''Test correct change'''
        price = 10
        tendered = 20
        actual = calculateChange(price, tendered)
        expected = [0, 1, 0, 0]
        self.assertListEqual(expected, actual)

    def test_basic_2(self):
        price = 74
        tendered = 100
        actual = calculateChange(price, tendered)
        expected = [1, 0, 1, 1]
        self.assertListEqual(expected, actual)

    def test_basic_3(self):
        price = 1
        tendered = 9999
        actual = calculateChange(price, tendered)
        expected = [499, 1, 1, 3]
        self.assertListEqual(expected, actual)

    def test_basic_4(self):
        price = 0
        tendered = 999999
        actual = calculateChange(price, tendered)
        expected = [49999, 1, 1, 4]
        self.assertListEqual(expected, actual)
#-----------------------------------------------
# Quadratic function tests
#-----------------------------------------------

class TestQuadraticFormula(unittest.TestCase):

    def test_simple(self):
        # basic test 1
        x1, x2 = simpleQuadraticEquation(9,12,4)
        self.assertAlmostEqual(x1, -2/3)
        self.assertAlmostEqual(x2, -2/3)

        # basic test 2
        x1, x2 = simpleQuadraticEquation(1, -2, -4)
        self.assertAlmostEqual(x1, 1 + math.sqrt(5))
        self.assertAlmostEqual(x2, 1 - math.sqrt(5))

        # zero test
        self.assertRaises(AssertionError, simpleQuadraticEquation,
                          0, 0, 0)

        self.assertRaises(AssertionError, simpleQuadraticEquation,
                          3, 4, 2)

    def test_advanced(self):
        # basic test 1
        x1, x2 = quadraticEquation(9,12,4)
        self.assertAlmostEqual(x1[0], -2/3)
        self.assertAlmostEqual(x1[1], 0.0)
        self.assertAlmostEqual(x2[0], -2/3)
        self.assertAlmostEqual(x2[1], 0.0)

        # basic test 2
        x1, x2 = quadraticEquation(1, -2, -4)
        self.assertAlmostEqual(x1[0], 1 + math.sqrt(5))
        self.assertAlmostEqual(x2[0], 1 - math.sqrt(5))
        self.assertAlmostEqual(x2[1], 0.0)
        self.assertAlmostEqual(x1[1], 0.0)

        # basic test 3
        x1, x2 = quadraticEquation(3, 4, 2)
        self.assertAlmostEqual(x1[0], -2/3)
        self.assertAlmostEqual(x2[0], -2/3)
        self.assertAlmostEqual(x2[1], -math.sqrt(2)/3)
        self.assertAlmostEqual(x1[1], math.sqrt(2)/3)

        # zero test
        self.assertRaises(AssertionError, quadraticEquation,
                          0, 0, 0)

def main():
    unittest.main()

if __name__ == "__main__":
    main()
